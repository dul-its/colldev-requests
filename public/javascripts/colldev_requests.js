$(function() {
  // increase maxlength of ISBN and ISSN fields to allow for extra characters.
  $('input#requestISBN').attr('maxlength', '20');
  $('input#requestISSN').attr('maxlength', '12');

  // set event to remove invalid characters from ISBN and ISSN on form submission
  $('form#requestTitleForm').submit(
    function() {
      var re = /[^\dx]+/g;
      $('input#requestISBN').val(
        function(i, value) {
          return value.replace(re, '');
        }
      );
      $('input#requestISSN').val(
        function(i, value) {
          return value.replace(re, '');
        }
      );
      return true;
    }
  );

  if( $('#from_datetimepicker').size() > 0) {

    var max_moment = moment.unix( $('#max_unixstamp').val() );
    $('#from_datetimepicker').datetimepicker({
      format: 'YYYY-MM-DD',
      useCurrent: false // Important!
    });
    $('#to_datetimepicker').datetimepicker({
      format: 'YYYY-MM-DD',
      maxDate: max_moment
    });

    $('#query_df').val( $('#from_datetimepicker').data('DateTimePicker').viewDate().unix() );
    console.log(" init: " + $('#query_df').val() );
    $('#query_dt').val( $('#to_datetimepicker').data('DateTimePicker').viewDate().unix() );
    console.log(" init: " + $('#query_dt').val() );

    $('#from_datetimepicker').on('dp.change', function (e) {
      $('#to_datetimepicker').data("DateTimePicker").minDate(e.date);
      $('#query_df').val( e.date.unix() );
    });

    $("#to_datetimepicker").on('dp.change', function (e) {
      //$('#from_datetimepicker').data("DateTimePicker").maxDate(e.date);
      $('#query_dt').val( e.date.unix() );
    });
  }

    // SEARCH TYPEAHEAD
    // --------------------------------------------------------------
    $('.librarian-typeahead').typeahead({

        minLength	: 0,

        items : 20,

        fitToElement: true,

        autoSelect: true,

        source: function(q, process) {
            var unix = Math.round(+new Date()/1000);
            return $.getJSON(
                '/librarian-typeahead/' + q + '?' + unix,
                function (data) {
                    return process(data);
                });
        },

        displayText: function(item) {
          console.log( item.name );
          if (item.initials == "") {
            return item.name;
          } else {
            return "[" + item.initials + "] " + item.name;
          }
        },
    });
    $('.librarian-typeahead').on('change', function(e) {
      var current = $(this).typeahead("getActive");
      var relField = $(this).data('rel');
      var relPersonDisplayName = $(this).data('rel-person');
      var relPersonId = $(this).data('rel-person-id');

      if (current) {
        $('#' + relField).val(current.id);
        $(this).val(current.initials);
        if (current.person_id != null && current.person_id != '') {
          $('#' + relPersonDisplayName).val(current.person_display_name);
          $('#' + relPersonId).val(current.person_id);
        }
        else {
          $('#' + relPersonDisplayName).val( current.initials ).parent().addClass('has-warning');
        }
      }
    });

    // PERSON TYPEAHEAD
    // --------------------------------------------------------------
    $('.person-typeahead').typeahead({

        minLength : 1,

        items : 20,

        fitToElement: true,

        autoSelect: false,

        changeInputOnSelect: false,

        changeInputOnMove: false,

        source: function(q, process) {
            var unix = Math.round(+new Date()/1000);
            return $.getJSON(
                '/person-typeahead/' + q + '?' + unix,
                function (data) {
                    return process(data);
                });
        },

        displayText: function(item) {
          if (item.is_librarian == '1')
            return item.name + ' *';

          return item.name;
        },

        afterSelect: function(current) {
        }
    });
    $('.person-typeahead').on('change', function() {
      var current = $(this).typeahead("getActive");
      var event = jQuery.Event("colldev.personSelected");
      event.personData = current;
      $('.librarian-info').trigger( event );
    });

    $('.librarian-info').on('colldev.personSelected', function(e) {
      var relData = $(this).data('rel');
      var personData = e.personData;
      var appendData = $(this).data('append');

      if (appendData) {
        newval = ( $(this).val().length > 0 ) ? $(this).val() + ', ' + personData[relData] : personData[relData];
      } else {
        $(this).val( personData[relData] );
      }
    });

    // COPY to CLIPBOARD (email records)
    $('.btn-copymsg').on('click', function(e) {
      var id = $(this).attr('data-rel');
      var el = document.getElementById( id );
      var range = document.createRange()
      range.selectNodeContents( el );
      var sel = window.getSelection();
      sel.removeAllRanges();
      sel.addRange( range );
      document.execCommand( 'copy' );
      alert( "Email message copied to clipboard." );
      return e.preventDefault();
    });

    var ADD_NEW_STATUS = -1;
    $('#newStatusContainer').hide();
    $('#requestStatus').on('change', function(e) {
      if ( $(this).val() == ADD_NEW_STATUS ) {
        $('#newStatusContainer').show();
      } else {
        $('#newStatusContainer').hide();
      }
    });
});
