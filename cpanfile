requires "Dancer2", ">= 0.300000, < 0.400000";
requires "TAP::Harness::Env";
requires "Module::Build::Tiny";
requires "Set::Infinite";
requires "DateTime";
requires "DateTime::Set";
requires "DateTime::Format::MySQL";
requires "Number::Format";
requires "Fatal";
requires "XML::SAX";
requires "Text::CSV";
requires "XML::LibXML";
#requires "ExtUtils::Embed";
requires "XML::Simple";
requires "DBIx::Class::ResultSet";
requires "Dancer2::Plugin::Ajax";
requires "Dancer2::Plugin::Database";
requires "Dancer2::Plugin::Deferred";
requires "Dancer2::Plugin::DBIC";
requires "Dancer2::Logger::Log4perl";
requires "Net::SSLeay";
requires "Dancer2::Plugin::Email";
requires "DBIx::Class::Result::Validation";
requires "Dancer2::Session::Memcached";
requires "DBD::mysql";
requires "Plack"						=> "1.0047";
requires "Task::Plack"                  => "0.28";
requires "Plack::Session";

recommends "Slack"            => "0.4015";
recommends "DBIx::Class::Schema::Loader";
recommends "YAML"             => "0";
recommends "URL::Encode::XS"  => "0";
recommends "CGI::Deurl::XS"   => "0";
recommends "HTTP::Parser::XS" => "0";
recommends "Plack::Handler::Apache2" => "0";

on "test" => sub {
    requires "Test::More"            => "0";
    requires "HTTP::Request::Common" => "0";
};
