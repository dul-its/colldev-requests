package colldev_requests::Email;

use strict;
use warnings;
use Carp;

use Dancer2 appname => 'colldev_requests';
use Dancer2::Plugin::Email;
use Dancer2::Plugin::DBIC;
use Dancer2::Plugin::Deferred;

use DateTime;

prefix '/admin/email-patron' => sub {
  get '' => sub {
    my $tid = query_parameters->get('tid') || undef;
    send_error "TitleRequest Id (tid) is required", 500 unless $tid;

    my $titlerequest = schema->resultset('Titlerequest')
      ->find( { id => $tid } );

    send_error "Request record not found!", 404 unless $titlerequest;

    my @emailrecords = $titlerequest->emailrecords->all;

    template 'email_patron' => { 
      page_title => 'Email Patron', 
      'tr' => $titlerequest,
      emailrecords => \@emailrecords,
    };
  };

  post '' => sub {
    my $form = body_parameters->as_hashref_mixed;
    send_error "'To:' is required!", 500 unless $form->{email_to};

    # Set the 'created_on' value for this new row
    my $now = DateTime->now(time_zone => 'America/New_York');
    $form->{recorded} = $now->strftime("%Y-%m-%d %H:%M:%S");

    email {
      to        => $form->{email_to},
      cc        => config->{colldev_email_address},
      from      => sprintf("do-not-reply@%s", request->env->{HTTP_HOST}),
      body      => $form->{message},
      type      => 'plain',
      subject   => $form->{email_subject},
    } or send_error "Unable to send email: $!", 500;

    my $titlerequest_id = $form->{title_request_id};

    delete $form->{email_to};
    delete $form->{email_subject};

    $form->{message} = sprintf("Email sent on: %s\n%s", $form->{recorded}, $form->{message});

    schema->resultset('Emailrecord')->create( $form )
      or send_error "Unable to save email record. However, the email was sent.", 500;

    deferred success => "Email sent.";
    redirect "/admin/request/" . $titlerequest_id;
  };
};

prefix '/email' => sub {
  hook before_template_render => sub {
    return unless request->path =~ /^\/email\/?/;
  };

  get '' => sub {
      email {
          to        => config->{colldev_email_test_address},
          from      => 'dlc32@duke.edu',
          cc        => 'dlc32@duke.edu',
          subject   => 'Test from colldev',
          body      => 'This is a test of the Collections Development system.',
          type      => 'html',
      } or send_error "Unable to send mail: $!", 500;

      template 'request_title' => { page_title => 'Email Me' };
  };
};

__END__
