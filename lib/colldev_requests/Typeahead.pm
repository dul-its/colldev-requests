package colldev_requests::Typeahead;

use strict;
use warnings;
use Carp;

use Dancer2 appname => 'colldev_requests';
use Dancer2::Plugin::Ajax;
use Dancer2::Plugin::Database;
use Dancer2::Plugin::DBIC;

use DateTime;

prefix '/librarian-typeahead' => sub {

  ajax ['get', 'post'] => '/:q' => sub {
    my $q = route_parameters->get('q');
    content_type 'application/json';

    my $sql = "SELECT '' as id, 'Choose Librarian Person...' as 'name', ? as initials, '' as label, '' as person_display_name, '' as person_id UNION " .
              "SELECT l.id, p.display_name as 'name', l.initials as initials, CONCAT_WS(' ', p.display_name, " .
              "CONCAT_WS('', '(', p.net_id,'/',l.initials,')')) as label, p.display_name as person_display_name, p.id as person_id " .
              "FROM librarian l " .
              "LEFT OUTER JOIN people p ON p.id = l.person_id " .
              "WHERE (INSTR(UPPER(l.initials), ?) <> 0) " .
              "ORDER BY label";

    my $lm_sth = database->prepare( $sql );
    $lm_sth->execute( $q, $q );
    my $results = $lm_sth->fetchall_arrayref( {} );
    to_json( $results );
  };
};

prefix '/person-typeahead' => sub {

  ajax ['get', 'post'] => '/:q?' => sub {
    my $q = lc route_parameters->get('q');
    debug "Q = " . $q;
    content_type 'application/json';

    my $sql = "SELECT" .
              "   l.id, " .
              "   p.display_name as 'name', " .
              "   p.display_name as display_name, " . 
              "   p.email as email, " .
              "   l.initials as initials, " .
              "   l.id as librarian_id, " .
              "   p.net_id as net_id, " .
              "   p.unique_id as unique_id, " .
              "   p.id as person_id, " .
              "   CASE WHEN l.id IS NULL THEN '0' ELSE '1' END as is_librarian " .
              "FROM people p " .
              "LEFT OUTER JOIN librarian l ON p.id = l.person_id " .
              "WHERE (INSTR(LOWER(l.initials), ?) <> 0) " .
              "   OR (INSTR(LOWER(p.display_name), ?) <> 0) " .
              "   OR (INSTR(LOWER(p.net_id), ?) <> 0) " .
              "ORDER BY display_name";

    my $lm_sth = database->prepare( $sql );
    debug $sql;
    $lm_sth->execute( $q, $q, $q );
    my $results = $lm_sth->fetchall_arrayref( {} );
    debug to_dumper $results;
    to_json( $results );
  };
};

__END__
