package colldev_requests::Request;

use strict;
use warnings;
use Carp;

use Dancer2 appname => 'colldev_requests';
use Dancer2::Plugin::DBIC;
use Dancer2::Plugin::Email;
use Dancer2::Plugin::Deferred;

use Data::Dumper;
use DateTime;

prefix '/request' => sub {
  hook before_template_render => sub {
    return unless request->path =~ /^(\/admin)?\/request\/?/;
    my $tokens = shift;

    my @formats = schema->resultset('Titleformat')->all;
    $tokens->{formats} = \@formats;

  };

  get '' => sub {
    #template 'request_title' => { page_title => 'Suggest A Purchase' };
    template 'index';
  };

  post '' => sub {
    template "index";
  };

  hook after => sub {
    return unless request->path =~ /^\/request\/?/;

  };
};


## ADMIN ROUTES 
## these should be protected by some authentication plugin
## (e.g. Shibboleth)

prefix '/admin/requests' => sub {
  get '' => sub {
    my $page = query_parameters->get('p') || 1;
    my $rows = config->{ui}->{rows_per_page};

    my $titlerequests = schema->resultset('Titlerequest')
      ->search( undef, {
          page => $page,
          rows => $rows,
          order_by => { -desc => 'created_on' },
        }
      );

    my $resultset_pager = $titlerequests->pager;

    template 'admin/requests' => {
      page_title => 'Purchase Requests',
      pager => $resultset_pager,
      titlerequests => $titlerequests,
    }
  };

  post '' => sub {
  };

};

prefix '/admin/request' => sub {
  get '/:id[Int]/email' => sub {
    my $id = route_parameters->get('id');

    my $titlerequest = schema->resultset('Titlerequest')->find({ id => $id });
    if ( !$titlerequest ) {
      send_error "Unable to locate purchase suggestion for (" . $id . ")", 404;
    }

    my $librarian_person;
    $librarian_person= schema->resultset('Person')->find( { id => $titlerequest->librarian->person_id } )
      if $titlerequest->librarian;

    my @emailrecords = $titlerequest->emailrecords->all;

    template "email_librarians" => {
      'tr' => $titlerequest,
      librarian => $librarian_person,
      page_title => $titlerequest->title,
      emailrecords => \@emailrecords,
    };

  };

  post '/:id[Int]/email' => sub {
    my $form = body_parameters->as_hashref_mixed;
    my $id = route_parameters->get('id');
    my $titlerequest = schema->resultset('Titlerequest')->find({ id => $id });

    email {
        to      => $form->{email_to},
        cc      => $form->{email_cc},
        from    => sprintf("do-not-reply@%s", request->env->{HTTP_HOST}),
        subject => 'FWD: ' . sprintf(config->{notify_functional_owner_subject}, $titlerequest->title),
        body    => $form->{email_message},
    };
    template 'email_librarians_success' => { page_title => 'Successful Request', 'tr' => $titlerequest };
  };

  get '/:id[Int]' => sub {
    my $id = route_parameters->get('id');

    my $titlerequest = schema->resultset('Titlerequest')->find({ id => $id });
    if ( !$titlerequest ) {
      debug sprintf( "Unable to locate purchase suggestion for (" . $id . ")" );
      send_error "Unable to locate purchase suggestion for (" . $id . ")", 404;
    }
    debug "Located purchase suggestion [titlerequest_id=" . $id . "]";

    my $librarian_person;
    $librarian_person= schema->resultset('Person')->find( { id => $titlerequest->librarian->person_id } )
      if $titlerequest->librarian;

    my @statuses = schema->resultset('Titlestatus')->all;
    my @librarians = schema->resultset('Librarian')->search();
    my @subject_areas = schema->resultset('SubjectArea')->all;

    my @emailrecords = $titlerequest->emailrecords->all;

    template 'admin/request_title' => { 
      page_title => $titlerequest->title, 
      'tr' => $titlerequest,
      statuses => \@statuses,
      librarians => \@librarians,
      subject_areas => \@subject_areas,
      librarian_person => $librarian_person,
      emailrecords => \@emailrecords,
    };
  };

  post '/:id[Int]' => sub {
    my $id = route_parameters->get('id');

    my $form = body_parameters->as_hashref_mixed;
    $form->{librarian_initials} |= '';
    $form->{librarian_id} |= '';

    my $titlerequest = schema->resultset('Titlerequest')->find({ id => $id });
    if ( !$titlerequest ) {
      error "Unable to locate purchase suggestion for (" . $id . ")";
      send_error "Unable to locate purchase suggestion for (" . $id . ")", 404;
    }
    debug "Located purchase suggestion for id = " . $id;

    my $button_action = exists $form->{submit} 
      ? "submit" 
      : ( exists $form->{email_librarian} ? 'email-librarian' : 'email-patron' );

    if ($button_action eq 'email-librarian') {
      redirect '/admin/request/' . $id . '/email', 301;
    } elsif ($button_action eq 'email-patron') {
      redirect '/admin/email-patron?tid=' . $titlerequest->id
    }

    delete $form->{submit};
    delete $form->{email_librarian};
    delete $form->{email_patron};

    # USER STORY when "Add new status" is selected from Status dropdown
    if ($form->{status_id} eq '-1') {
      $form->{new_status_value} =~ s/^\s+//g;
      $form->{new_status_value} =~ s/\s+$//g;

      # THIS WOULD BE A GOOD CANDIDATE FOR A SEPARATE sub {}
      if ($form->{new_status_value}) {
        my $titlestatus = schema->resultset('Titlestatus')->find_or_create(
        {
          value => $form->{new_status_value}
        });
        $form->{status_id} = $titlestatus->get_column('id');
      }
    }
    delete $form->{new_status_value};

    # balk when "librarian initials" is blank
    error sprintf("[TitleRequest:%s] Librarian's Initials (required) is blank!", $titlerequest->id)
      if !$form->{librarian_initials};

    send_error "Librarian's Initials is required", 500 if !$form->{librarian_initials};

    # create a Librarian record if "librarian_id" is not in the form data
    my $librarian = schema->resultset('Librarian')->find_or_create({
      id => $form->{person_id},
      initials => $form->{librarian_initials},
      person_id => $form->{person_id}
    });

    # remove these keys from the form hash, 
    # as we're going to save the title request next.
    delete $form->{librarian_initials};
    delete $form->{person_id};
    # save the titlerequest (notes, status changes);
    $titlerequest->update($form) or
      error sprintf("[TitleRequest:%s] Unable to save changes to record: %s", $titlerequest->id, $!)
        && send_error "Unable to save changes to this request record";
    
    deferred success => "Your changes have been saved.";
    redirect '/admin/request/' . $titlerequest->id;
  };

};

__END__
