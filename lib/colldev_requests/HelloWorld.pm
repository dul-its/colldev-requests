package colldev_requests::Email;

use strict;
use warnings;
use Carp;

use Dancer2 appname => 'colldev_requests';
use Dancer2::Plugin::Email;

use DateTime;

prefix '/email' => sub {
  hook before_template_render => sub {
    return unless request->path =~ /^\/email\/?/;
  };

  get '' => sub {
      email {
          to        => 'go.decro@gmail.com',
          from      => 'dlc32@duke.edu',
          cc        => 'dlc32@duke.edu',
          subject   => 'Test from colldev',
          body      => 'This is a test of the Collections Development system.',
          type      => 'html',
      };

      template 'request_title' => { page_title => 'Email Me' };
  };
};

__END__
