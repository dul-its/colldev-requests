use utf8;
package colldev_requests::Schema::Result::Librarian;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

colldev_requests::Schema::Result::Librarian

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<librarian>

=cut

__PACKAGE__->table("librarian");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 person_id

  data_type: 'bigint'
  is_foreign_key: 1
  is_nullable: 1

=head2 initials

  data_type: 'varchar'
  is_nullable: 0
  size: 5

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "person_id",
  { data_type => "bigint", is_foreign_key => 1, is_nullable => 1 },
  "initials",
  { data_type => "varchar", is_nullable => 0, size => 5 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<person_id>

=over 4

=item * L</person_id>

=back

=cut

__PACKAGE__->add_unique_constraint("person_id", ["person_id"]);

=head1 RELATIONS

=head2 titlerequests

Type: has_many

Related object: L<colldev_requests::Schema::Result::Titlerequest>

=cut

__PACKAGE__->has_many(
  "titlerequests",
  "colldev_requests::Schema::Result::Titlerequest",
  { "foreign.librarian_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2020-10-14 16:19:29
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:9tQaCr1nXftTQiUR65SO0A


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
