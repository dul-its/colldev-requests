use utf8;
package colldev_requests::Schema::Result::Titlerequest;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

colldev_requests::Schema::Result::Titlerequest

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<titlerequest>

=cut

__PACKAGE__->table("titlerequest");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 requester_name

  data_type: 'varchar'
  is_nullable: 0
  size: 255

=head2 requester_netid

  data_type: 'varchar'
  is_nullable: 0
  size: 20

=head2 requester_uniqueid

  data_type: 'varchar'
  is_nullable: 0
  size: 20

=head2 requester_affiliation

  data_type: 'varchar'
  is_nullable: 0
  size: 20

=head2 requester_dept

  data_type: 'varchar'
  is_nullable: 0
  size: 255

=head2 requester_address

  data_type: 'longtext'
  is_nullable: 0

=head2 requester_phone

  data_type: 'varchar'
  is_nullable: 0
  size: 80

=head2 requester_email

  data_type: 'varchar'
  is_nullable: 0
  size: 75

=head2 title

  data_type: 'varchar'
  is_nullable: 0
  size: 200

=head2 author

  data_type: 'varchar'
  is_nullable: 0
  size: 200

=head2 edition

  data_type: 'varchar'
  is_nullable: 0
  size: 100

=head2 format_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 other_format

  data_type: 'varchar'
  is_nullable: 0
  size: 100

=head2 volumes

  data_type: 'varchar'
  is_nullable: 0
  size: 100

=head2 isbn

  data_type: 'varchar'
  is_nullable: 0
  size: 13

=head2 issn

  data_type: 'varchar'
  is_nullable: 0
  size: 8

=head2 publisher

  data_type: 'varchar'
  is_nullable: 0
  size: 200

=head2 pub_place

  data_type: 'varchar'
  is_nullable: 0
  size: 100

=head2 pub_year

  data_type: 'varchar'
  is_nullable: 0
  size: 50

=head2 info_source

  data_type: 'varchar'
  is_nullable: 0
  size: 255

=head2 additional_info

  data_type: 'longtext'
  is_nullable: 0

=head2 notes

  data_type: 'longtext'
  is_nullable: 0

=head2 librarian_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 reported_affiliation

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 0
  size: 20

=head2 status_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 created_on

  data_type: 'datetime'
  datetime_undef_if_invalid: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "requester_name",
  { data_type => "varchar", is_nullable => 0, size => 255 },
  "requester_netid",
  { data_type => "varchar", is_nullable => 0, size => 20 },
  "requester_uniqueid",
  { data_type => "varchar", is_nullable => 0, size => 20 },
  "requester_affiliation",
  { data_type => "varchar", is_nullable => 0, size => 20 },
  "requester_dept",
  { data_type => "varchar", is_nullable => 0, size => 255 },
  "requester_address",
  { data_type => "longtext", is_nullable => 0 },
  "requester_phone",
  { data_type => "varchar", is_nullable => 0, size => 80 },
  "requester_email",
  { data_type => "varchar", is_nullable => 0, size => 75 },
  "title",
  { data_type => "varchar", is_nullable => 0, size => 200 },
  "author",
  { data_type => "varchar", is_nullable => 0, size => 200 },
  "edition",
  { data_type => "varchar", is_nullable => 0, size => 100 },
  "format_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "other_format",
  { data_type => "varchar", is_nullable => 0, size => 100 },
  "volumes",
  { data_type => "varchar", is_nullable => 0, size => 100 },
  "isbn",
  { data_type => "varchar", is_nullable => 0, size => 13 },
  "issn",
  { data_type => "varchar", is_nullable => 0, size => 8 },
  "publisher",
  { data_type => "varchar", is_nullable => 0, size => 200 },
  "pub_place",
  { data_type => "varchar", is_nullable => 0, size => 100 },
  "pub_year",
  { data_type => "varchar", is_nullable => 0, size => 50 },
  "info_source",
  { data_type => "varchar", is_nullable => 0, size => 255 },
  "additional_info",
  { data_type => "longtext", is_nullable => 0 },
  "notes",
  { data_type => "longtext", is_nullable => 0 },
  "librarian_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "reported_affiliation",
  { data_type => "varchar", default_value => "", is_nullable => 0, size => 20 },
  "status_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "created_on",
  {
    data_type => "datetime",
    datetime_undef_if_invalid => 1,
    is_nullable => 0,
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 emailrecords

Type: has_many

Related object: L<colldev_requests::Schema::Result::Emailrecord>

=cut

__PACKAGE__->has_many(
  "emailrecords",
  "colldev_requests::Schema::Result::Emailrecord",
  { "foreign.title_request_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 format

Type: belongs_to

Related object: L<colldev_requests::Schema::Result::Titleformat>

=cut

__PACKAGE__->belongs_to(
  "format",
  "colldev_requests::Schema::Result::Titleformat",
  { id => "format_id" },
  { is_deferrable => 1, on_delete => "RESTRICT", on_update => "RESTRICT" },
);

=head2 librarian

Type: belongs_to

Related object: L<colldev_requests::Schema::Result::Librarian>

=cut

__PACKAGE__->belongs_to(
  "librarian",
  "colldev_requests::Schema::Result::Librarian",
  { id => "librarian_id" },
  {
    is_deferrable => 1,
    join_type     => "LEFT",
    on_delete     => "RESTRICT",
    on_update     => "RESTRICT",
  },
);

=head2 status

Type: belongs_to

Related object: L<colldev_requests::Schema::Result::Titlestatus>

=cut

__PACKAGE__->belongs_to(
  "status",
  "colldev_requests::Schema::Result::Titlestatus",
  { id => "status_id" },
  { is_deferrable => 1, on_delete => "RESTRICT", on_update => "RESTRICT" },
);

=head2 titlerequest_subjects

Type: has_many

Related object: L<colldev_requests::Schema::Result::TitlerequestSubject>

=cut

__PACKAGE__->has_many(
  "titlerequest_subjects",
  "colldev_requests::Schema::Result::TitlerequestSubject",
  { "foreign.titlerequest_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2021-01-27 15:18:29
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:ms0KPy40FInOMLoC75DZiQ


__PACKAGE__->load_components(qw/ Result::Validation /);

sub _validate {
    my $self = shift;
    $self->validate_not_empty('requester_name');
    $self->validate_not_empty('requester_phone');
    $self->validate_not_empty('requester_dept');
    $self->validate_not_empty('requester_address');
    $self->validate_not_empty('title');
    $self->validate_not_empty('author');
    $self->validate_not_empty('format_id');
}

sub request_details {
  my $self = shift;

  my $d = "";
  $d .= "Title: " . $self->title . "\n";
  $d .= "Author(s): " . $self->author . "\n";
  $d .= "Format: " . $self->request_format() . "\n";
  $d .= "Year of Publication: " . $self->pub_year . "\n";
  $d .= sprintf("Requester Name: %s (%s)", $self->requester_name, $self->requester_netid) . "\n";
  $d .= "Email Address: " . $self->requester_email . "\n";
  $d .= "Telephone: " . $self->requester_phone . "\n";
  $d .= "Duke Department " . $self->requester_dept . "\n";
  $d .= "Duke Primary Affiliation " . $self->requester_affiliation . "\n";
  $d .= "Mailing Address:\n" . $self->requester_address . "\n";
  $d .= "Created On: " . $self->created_on . "\n";
  $d .= "ID: " . $self->id . "\n";

  return $d;
}

sub request_format {
  my $self = shift;

  if ( !defined $self->format ) {
    return "Unknown Format";
  }
  return $self->format->value;
}

sub request_status {
  my $self = shift;

  if ( !defined $self->status ) {
    return "Unknown Status";
  }
  return $self->status->value;
}

1;
