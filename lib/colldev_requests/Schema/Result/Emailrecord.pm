use utf8;
package colldev_requests::Schema::Result::Emailrecord;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

colldev_requests::Schema::Result::Emailrecord

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<emailrecord>

=cut

__PACKAGE__->table("emailrecord");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 title_request_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 message

  data_type: 'longtext'
  is_nullable: 0

=head2 recorded

  data_type: 'datetime'
  datetime_undef_if_invalid: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "title_request_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "message",
  { data_type => "longtext", is_nullable => 0 },
  "recorded",
  {
    data_type => "datetime",
    datetime_undef_if_invalid => 1,
    is_nullable => 0,
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 title_request

Type: belongs_to

Related object: L<colldev_requests::Schema::Result::Titlerequest>

=cut

__PACKAGE__->belongs_to(
  "title_request",
  "colldev_requests::Schema::Result::Titlerequest",
  { id => "title_request_id" },
  { is_deferrable => 1, on_delete => "RESTRICT", on_update => "RESTRICT" },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2020-10-02 11:33:23
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:/Wz5qayEuHDr6mxQBWoQIQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
