use utf8;
package colldev_requests::Schema::Result::TitlerequestSubject;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

colldev_requests::Schema::Result::TitlerequestSubject

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<titlerequest_subject>

=cut

__PACKAGE__->table("titlerequest_subject");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 titlerequest_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 subject_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "titlerequest_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "subject_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<titlerequest_id>

=over 4

=item * L</titlerequest_id>

=item * L</subject_id>

=back

=cut

__PACKAGE__->add_unique_constraint("titlerequest_id", ["titlerequest_id", "subject_id"]);

=head1 RELATIONS

=head2 subject

Type: belongs_to

Related object: L<colldev_requests::Schema::Result::Subject>

=cut

__PACKAGE__->belongs_to(
  "subject",
  "colldev_requests::Schema::Result::Subject",
  { id => "subject_id" },
  { is_deferrable => 1, on_delete => "RESTRICT", on_update => "RESTRICT" },
);

=head2 titlerequest

Type: belongs_to

Related object: L<colldev_requests::Schema::Result::Titlerequest>

=cut

__PACKAGE__->belongs_to(
  "titlerequest",
  "colldev_requests::Schema::Result::Titlerequest",
  { id => "titlerequest_id" },
  { is_deferrable => 1, on_delete => "RESTRICT", on_update => "RESTRICT" },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2020-10-02 11:33:23
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:GGpW22dVrF2n6qEtNjZxXA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
