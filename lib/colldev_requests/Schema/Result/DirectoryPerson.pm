use utf8;
package colldev_requests::Schema::Result::DirectoryPerson;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

colldev_requests::Schema::Result::DirectoryPerson

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<directory_person>

=cut

__PACKAGE__->table("directory_person");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 first_name

  data_type: 'varchar'
  is_nullable: 0
  size: 40

=head2 middle_name

  data_type: 'varchar'
  is_nullable: 1
  size: 40

=head2 last_name

  data_type: 'varchar'
  is_nullable: 0
  size: 40

=head2 nickname

  data_type: 'varchar'
  is_nullable: 1
  size: 40

=head2 display_name

  data_type: 'varchar'
  is_nullable: 1
  size: 80

=head2 net_id

  data_type: 'varchar'
  is_nullable: 0
  size: 20

=head2 unique_id

  data_type: 'varchar'
  is_nullable: 0
  size: 20

=head2 title

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 preferred_title

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 is_eg_member

  data_type: 'tinyint'
  is_nullable: 1

=head2 keywords

  data_type: 'longtext'
  is_nullable: 1

=head2 photo_url

  data_type: 'varchar'
  is_nullable: 1
  size: 200

=head2 ldap_dn

  data_type: 'varchar'
  is_nullable: 0
  size: 255

=head2 sap_org_unit

  data_type: 'varchar'
  is_nullable: 0
  size: 20

=head2 primary_affiliation

  data_type: 'varchar'
  is_nullable: 0
  size: 40

=head2 affiliation

  data_type: 'varchar'
  is_nullable: 0
  size: 255

=head2 campus_box

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 physical_address

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 email

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 fax

  data_type: 'varchar'
  is_nullable: 1
  size: 40

=head2 phone

  data_type: 'varchar'
  is_nullable: 1
  size: 40

=head2 phone2

  data_type: 'varchar'
  is_nullable: 1
  size: 40

=head2 url

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 email_privacy

  data_type: 'tinyint'
  is_nullable: 1

=head2 profile

  data_type: 'longtext'
  is_nullable: 0

=head2 office_hours

  data_type: 'longtext'
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "first_name",
  { data_type => "varchar", is_nullable => 0, size => 40 },
  "middle_name",
  { data_type => "varchar", is_nullable => 1, size => 40 },
  "last_name",
  { data_type => "varchar", is_nullable => 0, size => 40 },
  "nickname",
  { data_type => "varchar", is_nullable => 1, size => 40 },
  "display_name",
  { data_type => "varchar", is_nullable => 1, size => 80 },
  "net_id",
  { data_type => "varchar", is_nullable => 0, size => 20 },
  "unique_id",
  { data_type => "varchar", is_nullable => 0, size => 20 },
  "title",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "preferred_title",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "is_eg_member",
  { data_type => "tinyint", is_nullable => 1 },
  "keywords",
  { data_type => "longtext", is_nullable => 1 },
  "photo_url",
  { data_type => "varchar", is_nullable => 1, size => 200 },
  "ldap_dn",
  { data_type => "varchar", is_nullable => 0, size => 255 },
  "sap_org_unit",
  { data_type => "varchar", is_nullable => 0, size => 20 },
  "primary_affiliation",
  { data_type => "varchar", is_nullable => 0, size => 40 },
  "affiliation",
  { data_type => "varchar", is_nullable => 0, size => 255 },
  "campus_box",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "physical_address",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "email",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "fax",
  { data_type => "varchar", is_nullable => 1, size => 40 },
  "phone",
  { data_type => "varchar", is_nullable => 1, size => 40 },
  "phone2",
  { data_type => "varchar", is_nullable => 1, size => 40 },
  "url",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "email_privacy",
  { data_type => "tinyint", is_nullable => 1 },
  "profile",
  { data_type => "longtext", is_nullable => 0 },
  "office_hours",
  { data_type => "longtext", is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<ldap_dn>

=over 4

=item * L</ldap_dn>

=back

=cut

__PACKAGE__->add_unique_constraint("ldap_dn", ["ldap_dn"]);

=head2 C<net_id>

=over 4

=item * L</net_id>

=back

=cut

__PACKAGE__->add_unique_constraint("net_id", ["net_id"]);

=head2 C<unique_id>

=over 4

=item * L</unique_id>

=back

=cut

__PACKAGE__->add_unique_constraint("unique_id", ["unique_id"]);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2020-10-02 11:33:23
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:Z5TRMC2oOdIUh2EEXg+nnQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
