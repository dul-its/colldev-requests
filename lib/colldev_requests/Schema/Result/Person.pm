use utf8;
package colldev_requests::Schema::Result::Person;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

colldev_requests::Schema::Result::Person - VIEW

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';
__PACKAGE__->table_class("DBIx::Class::ResultSource::View");

=head1 TABLE: C<people>

=cut

__PACKAGE__->table("people");

=head1 ACCESSORS

=head2 id

  data_type: 'bigint'
  default_value: 0
  is_nullable: 0

=head2 first_name

  data_type: 'varchar'
  is_nullable: 1
  size: 40

=head2 middle_name

  data_type: 'varchar'
  is_nullable: 1
  size: 40

=head2 last_name

  data_type: 'varchar'
  is_nullable: 1
  size: 40

=head2 nickname

  data_type: 'varchar'
  is_nullable: 1
  size: 40

=head2 display_name

  data_type: 'varchar'
  is_nullable: 1
  size: 80

=head2 pronouns

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 net_id

  data_type: 'varchar'
  is_nullable: 1
  size: 20

=head2 unique_id

  data_type: 'varchar'
  is_nullable: 1
  size: 20

=head2 title

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 preferred_title

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 is_eg_member

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 1

=head2 photo_id

  data_type: 'integer'
  is_nullable: 1

=head2 photo_url

  data_type: 'varchar'
  is_nullable: 1
  size: 200

=head2 ldap_dn

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 sap_org_unit

  data_type: 'varchar'
  is_nullable: 1
  size: 20

=head2 primary_affiliation

  data_type: 'varchar'
  is_nullable: 1
  size: 40

=head2 affiliation

  data_type: 'varchar'
  is_nullable: 1
  size: 40

=head2 campus_box

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 physical_address

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 email

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 phone

  data_type: 'varchar'
  is_nullable: 1
  size: 40

=head2 email_privacy

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 1

=head2 profile

  data_type: 'text'
  is_nullable: 1

=head2 viewable

  data_type: 'tinyint'
  default_value: 1
  is_nullable: 1

=head2 orcid

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 libguide_id

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 libcal_id

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 slug

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 created_at

  data_type: 'datetime'
  datetime_undef_if_invalid: 1
  is_nullable: 0

=head2 updated_at

  data_type: 'datetime'
  datetime_undef_if_invalid: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "bigint", default_value => 0, is_nullable => 0 },
  "first_name",
  { data_type => "varchar", is_nullable => 1, size => 40 },
  "middle_name",
  { data_type => "varchar", is_nullable => 1, size => 40 },
  "last_name",
  { data_type => "varchar", is_nullable => 1, size => 40 },
  "nickname",
  { data_type => "varchar", is_nullable => 1, size => 40 },
  "display_name",
  { data_type => "varchar", is_nullable => 1, size => 80 },
  "pronouns",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "net_id",
  { data_type => "varchar", is_nullable => 1, size => 20 },
  "unique_id",
  { data_type => "varchar", is_nullable => 1, size => 20 },
  "title",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "preferred_title",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "is_eg_member",
  { data_type => "tinyint", default_value => 0, is_nullable => 1 },
  "photo_id",
  { data_type => "integer", is_nullable => 1 },
  "photo_url",
  { data_type => "varchar", is_nullable => 1, size => 200 },
  "ldap_dn",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "sap_org_unit",
  { data_type => "varchar", is_nullable => 1, size => 20 },
  "primary_affiliation",
  { data_type => "varchar", is_nullable => 1, size => 40 },
  "affiliation",
  { data_type => "varchar", is_nullable => 1, size => 40 },
  "campus_box",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "physical_address",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "email",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "phone",
  { data_type => "varchar", is_nullable => 1, size => 40 },
  "email_privacy",
  { data_type => "tinyint", default_value => 0, is_nullable => 1 },
  "profile",
  { data_type => "text", is_nullable => 1 },
  "viewable",
  { data_type => "tinyint", default_value => 1, is_nullable => 1 },
  "orcid",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "libguide_id",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "libcal_id",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "slug",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "created_at",
  {
    data_type => "datetime",
    datetime_undef_if_invalid => 1,
    is_nullable => 0,
  },
  "updated_at",
  {
    data_type => "datetime",
    datetime_undef_if_invalid => 1,
    is_nullable => 0,
  },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2020-10-14 16:21:01
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:RbJ8HpE/oLMWfTucdGKspQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
