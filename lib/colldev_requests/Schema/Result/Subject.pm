use utf8;
package colldev_requests::Schema::Result::Subject;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

colldev_requests::Schema::Result::Subject

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<subject>

=cut

__PACKAGE__->table("subject");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 value

  data_type: 'varchar'
  is_nullable: 0
  size: 40

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "value",
  { data_type => "varchar", is_nullable => 0, size => 40 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<value>

=over 4

=item * L</value>

=back

=cut

__PACKAGE__->add_unique_constraint("value", ["value"]);

=head1 RELATIONS

=head2 titlerequest_subjects

Type: has_many

Related object: L<colldev_requests::Schema::Result::TitlerequestSubject>

=cut

__PACKAGE__->has_many(
  "titlerequest_subjects",
  "colldev_requests::Schema::Result::TitlerequestSubject",
  { "foreign.subject_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2020-10-02 11:33:23
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:yqcRVrcrIpgsX7MOun+54A


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
