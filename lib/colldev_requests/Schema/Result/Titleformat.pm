use utf8;
package colldev_requests::Schema::Result::Titleformat;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

colldev_requests::Schema::Result::Titleformat

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<titleformat>

=cut

__PACKAGE__->table("titleformat");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 value

  data_type: 'varchar'
  is_nullable: 0
  size: 40

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "value",
  { data_type => "varchar", is_nullable => 0, size => 40 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<value>

=over 4

=item * L</value>

=back

=cut

__PACKAGE__->add_unique_constraint("value", ["value"]);

=head1 RELATIONS

=head2 titlerequests

Type: has_many

Related object: L<colldev_requests::Schema::Result::Titlerequest>

=cut

__PACKAGE__->has_many(
  "titlerequests",
  "colldev_requests::Schema::Result::Titlerequest",
  { "foreign.format_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2020-10-02 11:33:23
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:H4KLwrwPGLnkn5luMNEaTg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
