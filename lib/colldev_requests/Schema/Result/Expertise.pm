use utf8;
package colldev_requests::Schema::Result::Expertise;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

colldev_requests::Schema::Result::Expertise - VIEW

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';
__PACKAGE__->table_class("DBIx::Class::ResultSource::View");

=head1 TABLE: C<expertise>

=cut

__PACKAGE__->table("expertise");

=head1 ACCESSORS

=head2 id

  data_type: 'bigint'
  default_value: 0
  is_nullable: 0

=head2 person_id

  data_type: 'integer'
  is_nullable: 1

=head2 subject_area_id

  data_type: 'integer'
  is_nullable: 1

=head2 created_at

  data_type: 'datetime'
  datetime_undef_if_invalid: 1
  is_nullable: 0

=head2 updated_at

  data_type: 'datetime'
  datetime_undef_if_invalid: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "bigint", default_value => 0, is_nullable => 0 },
  "person_id",
  { data_type => "integer", is_nullable => 1 },
  "subject_area_id",
  { data_type => "integer", is_nullable => 1 },
  "created_at",
  {
    data_type => "datetime",
    datetime_undef_if_invalid => 1,
    is_nullable => 0,
  },
  "updated_at",
  {
    data_type => "datetime",
    datetime_undef_if_invalid => 1,
    is_nullable => 0,
  },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2020-10-14 16:21:01
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:SQJX6ei7IBXLUDNrbziFsQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
