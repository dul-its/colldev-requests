package colldev_requests::Reports;

use strict;
use warnings;
use Carp;

use Dancer2 appname => 'colldev_requests';
use Dancer2::Plugin::DBIC;

use DateTime;

prefix '/admin/reports' => sub {
  hook before_template_render => sub {
    return unless request->path =~ /^\/admin\/reports\/?/;

    my $tokens = shift;
    $tokens->{page_id} = 'reports';
  };

  get '' => sub {

    my $now = DateTime->now;
    $now->subtract ( days => 30 );

    my $titlerequest = schema->resultset('Titlerequest')->search(
      undef,
      {
        order_by => {-desc => 'created_on'},
        select => [
          { unix_timestamp => 'created_on', -as => 'max_unixstamp' },
        ]
      }
    )->first;

    my $max_unixstamp = DateTime->from_epoch( epoch => $titlerequest->get_column('max_unixstamp') );
    my $dt = $max_unixstamp->epoch();
    my $end_date = $max_unixstamp->ymd();

    my $df = DateTime->from_epoch( epoch => $titlerequest->get_column('max_unixstamp') )
      ->subtract( days => 30 );
    my $start_date = $df->ymd();

    my @statuses = schema->resultset('Titlestatus')->all;

    template 'admin/reports' => { 
      page_title => 'Reports: Purchase Requests',
      from_datetimepicker => $start_date,
      to_datetimepicker => $end_date,
      max_unixstamp => $titlerequest->get_column('max_unixstamp'),
      statuses => \@statuses,
      df => $df,
      dt => $dt,
    };
  };

  post '' => sub {
    my $start_date = body_parameters->get('start_date');
    my $end_date = body_parameters->get('end_date') || undef;
    my $df = body_parameters->get('df');
    my $dt = body_parameters->get('dt') || undef;
    my $ql = body_parameters->get('ql') || undef;
    my $qs = body_parameters->get('qs') || undef;

    my $dtf = schema->storage->datetime_parser;
    debug to_dumper {
      start_date => $start_date,
      end_date => $end_date,
      df => $df,
      dt => $dt,
      ql => $ql,
      qs => $qs,
    };

    my $crit = {};
    $crit->{created_on} = { 
      -between => [ 
        $dtf->format_datetime( DateTime->from_epoch( epoch => $df ) ), 
        $dtf->format_datetime( DateTime->from_epoch( epoch => $dt ) ),
      ] 
    } if $end_date;
    $crit->{created_on} = { '>=' => $start_date } if !$end_date;
    $crit->{status_id} = $qs if $qs;
    $crit->{librarian_id} = $ql if $ql;
    #debug to_dumper $crit;

    my $o = schema->resultset('Titlerequest')->search(
      $crit,
      {
        join => ['librarian', 'status'],
      }
    );

    my @rows = $o->all;

    debug to_dumper $o->as_query;
    #debug to_dumper shift @rows;

    my @table = ();
    push @table, [
      'requester_name',
      'requester_netid',
      'requester_uniqueid',
      'requester_affiliation',
      'reported_affiliation',
      'requester_dept',
      'requester_address',
      'requester_phone',
      'title',
      'author',
      'edition',
      'request_format',
      'other_format',
      'volumes',
      'isbn',
      'issn',
      'publisher',
      'pub_place',
      'pub_year',
      'info_source',
      'additional_info',
      'notes',
      'librarian_id',
      'request_status',
      'created_on',
    ];
    foreach (@rows) {
      push @table, [
        $_->requester_name,
        $_->requester_netid,
        $_->requester_uniqueid,
        $_->requester_affiliation,
        $_->reported_affiliation,
        $_->requester_dept,
        $_->requester_address,
        $_->requester_phone,
        $_->title,
        $_->author,
        $_->edition,
        $_->request_format(),
        $_->other_format,
        $_->volumes,
        $_->isbn,
        $_->issn,
        $_->publisher,
        $_->pub_place,
        $_->pub_year,
        $_->info_source,
        $_->additional_info,
        $_->notes,
        $_->librarian_id,
        $_->request_status(),
        $_->created_on,
      ];
    }
    #debug to_dumper \@table;

    my $now = DateTime->now;

    use Text::CSV;
    my $csv = Text::CSV->new({ binary => 1, auto_diag => 1 });

    open my $fh, ">:encoding(utf8)", "/tmp/newreport.csv" or die "newreport.csv: $!";
    $csv->say ($fh, $_) for @table;
    close $fh or die "newreport.csv: $!";

    send_file( '/tmp/newreport.csv',
               system_path => 1,
               content_type => 'text/csv',
               filename => 'colldev_requests_titlerequests.csv' );

  };
};

__END__
