package colldev_requests::RequestFormat;

use strict;
use warnings;
use Carp;

use Dancer2 appname => 'colldev_requests';
use Dancer2::Plugin::DBIC;
use Dancer2::Plugin::Deferred;

prefix '/admin/formats' => sub {
  get '' => sub {
    my $page = query_parameters->get('p') || 1;
    my $rows = config->{ui}->{rows_per_page};

    my $titleformat = schema->resultset('Titleformat')
      ->search( undef, {
          page => $page,
          rows => $rows,
          order_by => { -asc => 'value' },
        }
      );

    my $resultset_pager = $titleformat->pager;

    template 'admin/request_formats' => {
      page_title => 'Manage Formats',
      pager => $resultset_pager,
      titleformat => $titleformat,
    }
  };

  post '' => sub {
    my $form = body_parameters->as_hashref_mixed;
    my $page = $form->{current_page};

    delete $form->{current_page};

    my $format = schema->resultset('Titleformat')->create($form) or 
      send_error "Unable to save new format.";

    deferred success => "Your new format has been added.";
    redirect '/admin/formats?p=' . $page;
  };

};

prefix '/admin/format' => sub {
  get '/:id[Int]' => sub {
    my $id = route_parameters->get('id');

    my $titleformat = schema->resultset('Titleformat')->find({ id => $id });
    if ( !$titleformat ) {
      send_error "Unable to locate format for (" . $id . ")", 404;
    }

    template 'admin/request_format' => {
      page_title => 'Edit Format',
      'tf' => $titleformat,
    }
  };

  post '/:id[Int]' => sub {
    my $id = route_parameters->get('id');
    my $form = body_parameters->as_hashref_mixed;
    my $titleformat = schema->resultset('Titleformat')->find({ id => $id });

    delete $form->{submit};    

    $titleformat->update($form) or
      send_error "Unable to save changes to this format record";
    deferred success => "Your changes have been saved.";
    redirect '/admin/formats';
  };
};

#prefix '/admin/format' => {
  #get '/:id[Int]' => sub {
  #  my $id = route_parameters->get('id');

  #  my $titleformat = schema->resultset('Titleformat')->find({ id => $id });
  #  if ( !$titleformat ) {
  #    send_error "Unable to locate format for (" . $id . ")", 404;
  #  }

  #  template 'admin/request_format' => { 
  #    page_title => $titleformat->value, 
  #    'tf' => $titleformat,
  #  };
  #};

  #post '/:id[Int]' => sub {

  #};
#};