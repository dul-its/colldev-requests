package colldev_requests::RequestStatus;

use strict;
use warnings;
use Carp;

use Dancer2 appname => 'colldev_requests';
use Dancer2::Plugin::DBIC;
use Dancer2::Plugin::Deferred;

use DateTime;

prefix '/admin/statuses' => sub {
  get '' => sub {
    my $page = query_parameters->get('p') || 1;
    my $rows = config->{ui}->{rows_per_page};

    my $titlestatus = schema->resultset('Titlestatus')
      ->search( undef, {
          page => $page,
          rows => $rows,
          order_by => { -asc => 'value' },
        }
      );

    my $resultset_pager = $titlestatus->pager;

    template 'admin/request_statuses' => {
      page_title => 'Manage Statuses',
      pager => $resultset_pager,
      titlestatus => $titlestatus,
    }
  };
};