package colldev_requests;
use Dancer2;
use Dancer2::Plugin::Deferred;
use Dancer2::Plugin::DBIC;

use colldev_requests::Request;
use colldev_requests::RequestStatus;
use colldev_requests::RequestFormat;
use colldev_requests::Email;
use colldev_requests::Reports;
use colldev_requests::Typeahead;

our $VERSION = '1.40';

# Use this hook for request-level processing.
# This will run PRIOR to the execution of the route(s) below.
# 
# Possible use-cases for this hook:
# - initialize (authenticated) user session (post-Shibboleth)
hook before => sub {
  my $netid = config->{default_uid};
  my %requester;

  if (request->path =~ /^\/guest-warning/) {
    return;
  }

  ## debug to_dumper session('requester');

  my $force_admin_ui = config->{force_admin_ui} || 0;
  var is_admin => (request->env->{HTTP_ISMEMBEROF} =~ /colldev-requests-admin/) || $force_admin_ui;

  if (!session('requester')) {
    $netid |= (request->env->{uid} || request->env->{HTTP_UID});
  } else {
    $netid = session('requester')->{net_id};
  }

  # attempt to find the person in the staff directory
  # (this works for Perkins staff, but not professional library staff)
  my $requester_result = schema->resultset("Person")->find( { net_id => $netid } );
  if ($requester_result) {
    %requester = $requester_result->get_columns();
  } else {
    # create a fake record for UI usage.
    %requester = (
      "net_id" => $netid,
      "unique_id" => request->env->{dudukeid} || request->env->{HTTP_DUDUKEID} || "",
      "affiliation" => request->env->{primary_affiliation} || request->env->{HTTP_PRIMARY_AFFILIATION} || "",
      "display_name" => request->env->{displayname} || request->env->{HTTP_DISPLAYNAME} || "",
      "email" => request->env->{mail} || request->env->{HTTP_MAIL} || "",
      "phone" => request->env->{telephonenumber} || request->env->{HTTP_TELEPHONENUMBER} || "",
      "physical_address" => "",
    );
  }

  var requester => \%requester;
  session requester => \%requester if !session('requester');

  if (request->path =~ /^\/admin/) {
    if (!var 'is_admin') {
      send_error "Access to /admin functions not allowed", 403;
    }
  }
};

# Use this hook to finalize template (context) variables 
# before the output is rendered.
#
# This is synomymous with Drupal's "preprocess" hooks
hook before_template_render => sub {
  my $tokens = shift;

  $tokens->{requester} = vars->{requester};
  $tokens->{VERSION} = $VERSION;
  $tokens->{DANCER_ENVIRONMENT} = request->env->{DANCER_ENVIRONMENT};
  # $token->{title} = 'Collections Development Requests';
  
  if ($tokens->{pager}) {
    use List::Util qw[min max];

    my $Z = $tokens->{pager}->current_page;
    my $Q = config->{ui}->{pages_per_chapter};
    my $r = int(($Z-1)/$Q);
    my $begin = $Q*$r + 1;
    my $end = min($Q*($r+1), $tokens->{pager}->last_page);

    $tokens->{pager}->{page_begin} = $begin;
    $tokens->{pager}->{page_end} = $end;
    $tokens->{pager}->{working_url} = sprintf("%s%s", request->uri_base, request->path);
    
    # any non-zero value will satisy 'truth' here
    $tokens->{pager}->{show_page_one} = $r;
    
    $tokens->{pager}->{current_chapter} = $r + 1;
  }

  #debug to_dumper $tokens->{deferred};
};

# Use this hook for post-processing AFTER the response 
# has been created.
#
# Use-case is to clear session variables
hook after => sub {

};

# ROUTES (or CONTROLLERS)
# ------------------------------
#
# The only primary UI function for this web application 
# is to display a web form to gather 'title request' data 
# from a NetID-authenticated individual.
#
# With that in mind, the 'index' template will (or should) only 
# display the form.
get '/' => sub {
    template 'index';
    #redirect '/request', 301;
};

get '/about' => sub {
  template 'about' => { page_title => 'About This Site' };
};

get '/guest-warning' => sub {
  template 'guest-warning' => { page_title => 'Restricted Access' };
};

get '/logout' => sub {
  app->destroy_session;
  redirect '/Shibboleth.sso/Logout';
};

true;

__END__

=encoding UTF-8

=head1 NAME

colldev_requests.pm - Route Controller for Collections Development Request Forms

=cut
