#!/usr/bin/env perl

use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin/../lib";

use colldev_requests;
use Plack::Builder;

builder {
    # enable static file handling of "/images/media", typically not 
    # present under the project's "/public" folder.
    #
    # See: https://metacpan.org/pod/release/MIYAGAWA/Plack-0.9974/lib/Plack/Middleware/Static.pm#CONFIGURATIONS
    # regarding the munging of $_ in a subroutine in order to 
    # produce the correct path for 'root'
    # enable "Plack::Middleware::Static",
    #     path => sub { s!^/images/media/!! }, root => colldev_requests->config->{areamap_root} . '/';

    # after handling all static paths that require attention, 
    # pass the PSGI app instance
    colldev_requests->to_app;
}

# When not using the Builder setup, uncomment this line
#locguide->to_app;
