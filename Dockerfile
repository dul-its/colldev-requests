FROM perl:latest

RUN yum install devtoolset-8
RUN yum install perl-core libtemplate-perl zlib-devel
RUN yum install -y openssl-devel
